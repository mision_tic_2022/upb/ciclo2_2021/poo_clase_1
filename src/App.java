public class App {
    public static void main(String[] args) throws Exception {
        //Construcción de un objeto de tipo Persona
        Persona objPersona_1 = new Persona("Juan", "Perez", 20, "12345");
        
        //Acceder al nombre del objeto
        System.out.println("Nombre: "+objPersona_1.getNombre());
        System.out.println("Apellido: "+objPersona_1.getApellido());
        System.out.println("---------Datos modificados----------");
        objPersona_1.setNombre("Andrés");
        objPersona_1.setApellido("Quintero");
        System.out.println("Nombre: "+objPersona_1.getNombre());
        System.out.println("Apellido: "+objPersona_1.getApellido());

        System.out.println("--------------OBJPERSONA 2----------");
        Persona objPersona_2 = new Persona("Ana", "Herrera", 21, "987654");
        System.out.println("Nombre: "+objPersona_2.getNombre());

    }
}
